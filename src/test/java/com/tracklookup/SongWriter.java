package com.tracklookup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.EndOfLeafException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;

public class SongWriter {


	@Test
	public void test() throws IOException, EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException {
		File channelsFile = new File("./src/test/resources/channels-generated.txt");
		BufferedWriter br = new BufferedWriter(new FileWriter(channelsFile));
		StringBuilder channels = new StringBuilder();

		File songsFile = new File("./src/test/resources/songs-generated.txt");
		BufferedWriter br2 = new BufferedWriter(new FileWriter(songsFile));
		StringBuilder songs = new StringBuilder();

		Random random = new Random();

		int volume = 80000;

		Map<Integer, Character> converter = new HashMap<Integer, Character>();
		converter.put(1, 'A');
		converter.put(2, 'B');
		converter.put(3, 'C');
		converter.put(4, 'D');
		converter.put(5, 'E');
		converter.put(6, 'F');
		converter.put(7, 'G');
		converter.put(8, 'H');
		converter.put(9, 'I');
		converter.put(10, 'J');
		converter.put(11, 'K');
		converter.put(12, 'L');

		for(int id = 0; id < volume; id++) {
			String channel = "";

			/*verse*/
			String verse = "";
			for(int i = 0; i < 4; i++) {
				String bar = "";
				for(int note = 0; note < 16; note++){
					bar += converter.get(random.nextInt(12) + 1);
				}
				verse += bar;
			}

			/*chorus */
			String chorus = "";
			for(int i = 0; i < 4; i++) {
				String bar = "";
				for(int note = 0; note < 16; note++){
					bar += converter.get(random.nextInt(12) + 1);
				}
				chorus += bar;
			}

			channel = verse + chorus + verse + chorus + chorus + verse + chorus + chorus + chorus;
			channels.append(id + "#" + id + "#" + channel + System.lineSeparator());
			songs.append(id + "#Test#Channel" + id + System.lineSeparator());
		}

		br.write(channels.toString());
		br.close();

		br2.write(songs.toString());
		br2.close();
	}
}
