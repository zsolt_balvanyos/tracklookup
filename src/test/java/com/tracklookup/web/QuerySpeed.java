package com.tracklookup.web;

import com.tracklookup.Application;
import com.tracklookup.Song;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Zsolt on 31-Aug-16.
 */


@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class QuerySpeed {

    @Autowired
    UserServices userServices;

    @Test
    public long measureQuerySpeed() {
        long start = System.currentTimeMillis();
        List<Song> songs = new ArrayList<>();
        songs.addAll(userServices.evaluate(new ArrayList<>(Arrays.asList(65,74,81,85,90,95,100,111,120,125,128,138,143,146,152,154,158,159,167,175,178,179,189))));
        long duration = System.currentTimeMillis() - start;
        System.out.println(songs + "\n Found in " + duration + "ms.");

        Song song = new Song("Test","TestSong");
        assertEquals(songs.get(0),song);

        return duration;
    }
}
