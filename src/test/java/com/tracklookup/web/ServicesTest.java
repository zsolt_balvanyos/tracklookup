package com.tracklookup.web;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tracklookup.Application;
import org.apache.log4j.*;
import org.junit.Before;
import org.junit.Test;

import com.tracklookup.Song;
import com.tracklookup.suffixtree.Root;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class ServicesTest {
	String fs = File.separator;
	String MIDIFILES = "." + fs + "src" + fs + "test" + fs + "resources" + fs + "midi" + fs;

	@Autowired
	Root root;

	@Autowired
	UserServices userServices;

	@Autowired
	AdminService adminService;

	@Before
	public void adminService() throws IOException {
		Logger logger = LogManager.getLogger(ServicesTest.class);
		logger.setLevel(Level.DEBUG);

		File cali = new File(MIDIFILES + "californication.mid");
		File bridge = new File(MIDIFILES + "under_the_bridge.mid");
		File dani = new File(MIDIFILES + "dani_california.mid");
		File scar = new File(MIDIFILES + "scar_tissue.mid");

		adminService.addSong(cali, "RHCP", "Californication");
		adminService.addSong(bridge, "RHCP", "Under the Bridge");
		adminService.addSong(dani, "RHCP", "Dani California");
		adminService.addSong(scar, "RHCP", "Scar Tissue");

		if(false) {
			String result = root.testPrint(adminService.channelTable);
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File("." + fs + "log" + fs + "suffixTree.txt")));
			bw.write(result);
			bw.flush();
			bw.close();
		}
	}

	@Test
	public void userService() {
		try {
			Song californication = new Song("RHCP", "Californication");
			List<Song> caliGood = new ArrayList<>();
			caliGood.addAll(userServices.evaluate(Arrays.asList(5, 2, 0, 9, 0, 4, 11, 0)));
			List<Song> caliFaulty = new ArrayList<>();
			caliFaulty.addAll(userServices.evaluate(Arrays.asList(5, 2, 1, 9, 0, 4, 11, 0)));
			assertEquals(californication, caliGood.get(0));
			assertEquals(californication, caliFaulty.get(0));
			
//			Song underTheBridge = new Song("RHCP", "Under the Bridge");
//			List<Song> bridgeGood = userServices.evaluate(Arrays.asList(11, 3, 6, 11, 8, 1, 4, 8, 1, 4, 11, 8, 4, 8, 4));
//			List<Song> bridgeFaulty = userServices.evaluate(Arrays.asList(11, 3, 6, 4, 7, 1, 4, 8, 1, 4, 11, 8, 4, 8, 4));
//			assertEquals(underTheBridge, bridgeGood.get(0));
//			assertEquals(underTheBridge, bridgeFaulty.get(0));
//
//			Song daniCalifornia = new Song("RHCP", "Dani California");
//			List<Song> daniGood = userServices.evaluate(Arrays.asList(2, 2, 7, 7, 9, 5, 9, 9, 9, 9, 9, 9, 4, 9));
//			List<Song> daniFaulty = userServices.evaluate(Arrays.asList(2, 7, 7, 9, 5, 9, 9, 9, 9, 9, 4, 9));
//			assertEquals(daniCalifornia, daniGood.get(0));
//			assertEquals(daniCalifornia, daniFaulty.get(0));
//
//			Song scarTissue = new Song("RHCP", "Scar Tissue");
//			List<Song> scarGood = userServices.evaluate(Arrays.asList(0, 2, 7, 5, 7, 5, 4, 5, 5, 9, 5));
//			List<Song> scarFaulty = userServices.evaluate(Arrays.asList(1, 2, 7, 5, 7, 5, 4, 7, 5, 9, 5));
//			assertEquals(scarTissue, scarGood.get(0));
//			assertEquals(scarTissue, scarFaulty.get(0));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
