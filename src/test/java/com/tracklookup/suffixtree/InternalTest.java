package com.tracklookup.suffixtree;

import static org.junit.Assert.*;

import com.tracklookup.Application;
import com.tracklookup.Channel;
import com.tracklookup.utility.NodeFactory;
import org.junit.Before;
import org.junit.Test;

import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class InternalTest {
	Internal internal;

	private int END = 11;
	private int trackId;
	private String TRACK= "adfasdfaefae";

	@Autowired
	ChannelTable tracktable;

	@Autowired
	Root root;

	@Autowired
	NodeFactory nodeFactory;
	
	@Before
	public void setup() throws ParentOfRootRequestedException, IOException {
		int trackId = tracktable.saveChannel(1, TRACK);
		internal = nodeFactory.getInternal(4, 7, root, trackId);
	}

	@Test
	public void getLength() {
		assertEquals(3, internal.getLength());
	}

	@Test
	public void getNode() throws ChildNotFoundException {
		Leaf leaf = nodeFactory.getLeaf(7, END, internal, trackId);
		internal.setNode('a', leaf);
		Node result = internal.getNode('a');
		
		assertEquals(leaf, result);
	}
}
