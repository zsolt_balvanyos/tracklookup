package com.tracklookup.suffixtree;

import static org.junit.Assert.*;

import com.tracklookup.Application;
import org.junit.Before;
import org.junit.Test;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class RootTest {

	@Autowired
	Root root;

	@Test
	public void getLength() {
		assertEquals(0, root.getLength());
	}
	
	@Test
	public void getLink() {
		assertEquals(root, root.getLink());
	}

	@Test (expected = ParentOfRootRequestedException.class)
	public void getParent() throws ParentOfRootRequestedException {
		root.getParent();
	}
	
	@Test
	public void getTrackID() {
		assertEquals(0, root.getChannelId());
	}
}