package com.tracklookup.suffixtree;

import com.tracklookup.Channel;
import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.EndOfLeafException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.*;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.tracklookup.Application;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(MockitoJUnitRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class SuffixTreeTest {
	String fs = File.separator;
	ActivePoint activePoint;
	Logger logger = LogManager.getLogger(SuffixTreeTest.class);

	@Autowired
	Root root;

	@Mock
	ChannelTable channelTable;

	@InjectMocks
	@Resource
	SuffixTree suffixTree;

	Channel channel;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		String notes = "abcdbcf";
		channel = new Channel(1,1,notes);
	}
	@Test
	public void addTrack() throws EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException, IOException {

		Mockito.when(channelTable.getChannel(1)).thenReturn(channel);

		suffixTree.addChannel(1);

		root.print(channelTable);






//		String two = "ccccd";
//		String three = "abcdbccccfx";
//		String four = "RUUUDDUDDRRUUUDDUDDRX";
//		String five = "RUUUDDUDDRRUUUDDUDDRXRUUUDDUDDRRUUUDDUDDRXSM";
//		channelTable.saveChannel(1, one);
//		channelTable.saveChannel(2, two);
//		channelTable.saveChannel(3, three);
//		channelTable.saveChannel(4, four);
//		int trackId = channelTable.saveChannel(5, five);

//		manager.addTrack(1);
//		manager.addTrack(2);
//		manager.addTrack(3);
//		manager.addTrack(4);
//		suffixTree.addChannel(trackId);
//		root.print(channelTable);
//
//		File file = new File("." + fs + "src" + fs + "test" + fs + "resources" + fs + "baseline.txt");
//		BufferedReader br = new BufferedReader(new FileReader(file));
//
//		String baseline = "";
//		String line = "";
//		while((line = br.readLine()) != null) {
//			baseline += line + "\n";
//		}
//		br.close();
//		assertEquals(baseline, root.testPrint(channelTable));
	}
}
