package com.tracklookup.suffixtree;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Map;

import com.tracklookup.Application;
import com.tracklookup.Channel;
import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.EndOfLeafException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import org.apache.log4j.*;
import org.junit.Test;

import com.tracklookup.ChannelTable;
import com.tracklookup.utility.Factory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class SuffixTest {

	@Autowired
	Factory factory;

	@Autowired
	ChannelTable channelTable;

	@Autowired
	Root root;

	@Autowired
	SuffixTree tree;



	@Test
	public void test1() throws Exception {
		Map<Integer, Channel> map = channelTable.getMap();

		for(int i: channelTable.getMap().keySet()) {
			tree.addChannel(i);
		}
		
		for(int i: channelTable.getMap().keySet()) {
			String str = map.get(i).getNotes();
			int index = 0;
			ActivePoint activePoint = factory.getActivePoint(root, 0);
				
			while(index < str.length() && activePoint.hasChar(str.charAt(index))) {
				activePoint.incrementDepth();
				index++;
			}
			
			assertEquals(str, str.substring(0, index));
		}
	}

	@Test
	public void underTheBridge() throws IOException, ParentOfRootRequestedException, EndOfLeafException, ChildNotFoundException {
		BasicConfigurator.configure(new FileAppender(new PatternLayout(), "./log/log.txt"));
		Logger logger = LogManager.getLogger(SuffixTest.class);
		logger.setLevel(Level.DEBUG);

		int t1Id = channelTable.saveChannel(1,"ACADAEAFACADAEAAACAFAHAAACAFAHAAACAFAHAAACAFAHACAJAFADFFFCACACACACACACADADAAACACADAHAJAFADAHAJAJAJAJAJACAHAJACDDDEAJAFADFFFCACACACACACACADADAAACACADAHAJAFADAHAJAJAJAJAJACAHAJACAHAJAJAHACAJAHACAJAHACAJAHACAJAHACAJAAAJAFADAHAJAJAJAJAJAHACADADAEAJHACADAHAJAFADAHAJAJAJAJAJACAHAJAJAHDDHEFJJAFADFFFCACACACACACACADADAAACACADAHAJAJAAAJAJAAAJAJAAAJAJAAAJAJAAAJAJAAAJAJAJACADACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAJACADADAAAAAAACAJAJAJAAAAABAAABACAAADACAAAJACACADA");
		int t2Id = channelTable.saveChannel(1,"ACADAEAFACADAEAAACAFAHAAACAFAHAAACAFAHAAACAFAHAHAEABABAEAEABABAFAAAAAJACAAAAACAJAAAAAJACAAAAHAHHAJAFADFFFCACACACACACACADADAAACACADAHAJAFADAHAJAJAJAJAJACAHAJACDDDEAJAFADFFFCACACACACACACADADAAACACADAHAJAFADAHAJAJAJAJAJACAHAJACAHAJAJAHACAJAHACAJAHACAJAHACAJAHACAJAAAJAFADAHAJAJAJAJAJAHACADADAEAJHACADAHAJAFADAHAJAJAJAJAJACAHAJAJAHDDHEFJJAFADFFFCACACACACACACADADAAACACADAHAJAJAAAJAJAAAJAJAAAJAJAAAJAJAAAJAJAAAJAJAJACADACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAJACADADAAAAAAACAJAJAJAAAAABAAABACAAADACAAAJACACADA");

		tree.addChannel(t1Id);
		logger.info("=================================================");
		tree.addChannel(t2Id);
	}
}
