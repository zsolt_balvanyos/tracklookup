package com.tracklookup.suffixtree;

import static org.junit.Assert.*;

import com.tracklookup.Application;
import com.tracklookup.ChannelTable;
import com.tracklookup.utility.Factory;
import com.tracklookup.utility.NodeFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PatternLayout;
import org.junit.Before;
import org.junit.Test;

import com.tracklookup.exceptions.EndOfLeafException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class ActivePointTest {
	Leaf leaf;
	Internal internal;
	ActivePoint activePoint;
	final static private int SONGID = 1;
	final static private int END = 11;
	final static private String TRACK= "adfasdfaefae";
	//									0123456789

	@Autowired
	Root root;

	@Autowired
	Factory factory;

	@Autowired
	NodeFactory nodeFactory;

	@Autowired
	ChannelTable trackTable;
	
	@Before
	public void setup() throws IOException {
		BasicConfigurator.configure(new FileAppender(new PatternLayout(), "./log/log.txt"));
		int trackId = trackTable.saveChannel(SONGID, TRACK);
		internal = nodeFactory.getInternal(4, 7, root, trackId);
		leaf = nodeFactory.getLeaf(7, END, internal, trackId);
		internal.setNode('a', leaf);
	}
	
	@Test
	public void hasEgde_TRUE_whenNOTatTheEnd() throws EndOfLeafException {
		activePoint = factory.getActivePoint(internal, 2);
		assertTrue(activePoint.hasChar('f'));
		assertEquals(internal, activePoint.getActiveNode());
		assertEquals(2, activePoint.getDepth());
	}

	@Test
	public void hasEgde_TRUE_whenAtTheEnd() throws EndOfLeafException {
		activePoint = factory.getActivePoint(internal, 3);
		assertTrue(activePoint.hasChar('a'));
		assertEquals(leaf, activePoint.getActiveNode());
		assertEquals(0, activePoint.getDepth());
	}

	@Test
	public void hasEgde_FALSE_whenNOTatTheEnd() throws EndOfLeafException {
		activePoint = factory.getActivePoint(internal, 2);
		assertFalse(activePoint.hasChar('x'));
	}

	@Test
	public void hasEgde_FALSE_whenAtTheEnd() throws EndOfLeafException {
		activePoint = factory.getActivePoint(internal, 3);
		assertFalse(activePoint.hasChar('x'));
	}
}
