//package com.tracklookup.suffixtree;
//
//
//import static org.junit.Assert.assertEquals;
//
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import org.junit.Before;
//import org.junit.Test;
//
//import com.tracklookup.TrackTable;
//import com.tracklookup.exceptions.ChildNotFoundException;
//import com.tracklookup.exceptions.EndOfLeafException;
//import com.tracklookup.exceptions.ParentOfRootRequestedException;
//
//public class SuffixHelperMethodsTest {
//	Leaf leaf1;
//	Leaf activeNode;
//	Internal internal1;
//	Internal internal2;
//	Internal internal3;
//	ActivePoint activePoint;
//	SuffixTree tree;
//	Logger logger;
//	final static private int TRACKID = 8;
//	final static private String TRACK= "adfasdfaefaek";
//									  //012345678901
//	TrackTable tracktable;
//
//	@Before
//	public void setup() {
//		logger = LogManager.getLogger(SuffixHelperMethodsTest.class);
//
//		TrackTable.getInstance().addTrack(TRACKID, TRACK);
//
//		internal1 = new Internal(0, 5, Root.getInstance(), TRACKID);
//		internal2 = new Internal(5, 6, internal1, TRACKID);
//		internal3 = new Internal(6, 9, internal2, TRACKID);
//		leaf1 = new Leaf(9, 11, internal3, TRACKID);
//		activeNode = new Leaf(5, 11, Root.getInstance(), TRACKID);
//
//		Root.getInstance().setNode(internal1);
//		internal1.setNode(internal2);
//		internal2.setNode(internal3);
//		internal3.setNode(leaf1);
//	}
//
//	@Test
//	public void walkDown_whenPassMultipleNodes() throws EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException {
//		activePoint = new ActivePoint(activeNode, 5);
//		tree = new SuffixTree();
//		assertEquals(leaf1, tree.walkDown(internal2, 6, TRACKID, 11).getActiveNode());
//	}
//
//	@Test
//	public void walkDown_whenGoToNextNode() throws EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException {
//		activePoint = new ActivePoint(activeNode, 3);
//		tree = new SuffixTree();
//		assertEquals(internal3, tree.walkDown(internal2, 3, TRACKID, 11).getActiveNode());
//	}
//
//	@Test
//	public void walkDown_whenStayOnSameNode() throws EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException {
//		activePoint = new ActivePoint(activeNode, 1);
//		tree = new SuffixTree();
//		assertEquals(internal2, tree.walkDown(internal2, 1, TRACKID, 11).getActiveNode());
//	}
//
//	@Test (expected = EndOfLeafException.class)
//	public void walkDown_whenReachingEndOfLeaf() throws EndOfLeafException, ChildNotFoundException, ParentOfRootRequestedException {
//		activePoint = new ActivePoint(activeNode, 11);
//		tree = new SuffixTree();
//		assertEquals(leaf1, tree.walkDown(internal2, 7, TRACKID, 12).getActiveNode());
//	}
//
//	@Test
//	public void insert() throws ParentOfRootRequestedException, ChildNotFoundException, EndOfLeafException {
//		logger.info(Root.getInstance().testPrint());
//		activePoint = new ActivePoint(internal3, 2);
//		tree = new SuffixTree();
//
//		Internal result = (Internal) tree.insertNode(internal3, TRACKID, 4, 1, null);
//		Internal inner = new Internal(internal3.getStart(), internal3.getStart() + 2, internal3.getParent(), TRACKID);
//		Leaf outer = new Leaf(internal3.getStart() + 2, 11, inner, TRACKID);
//		Leaf leaf = new Leaf(4, 11, inner, TRACKID);
//
//		assertEquals(internal3.getParent(), result);
//		assertEquals(inner, result.getNodes().get('f'));
//		assertEquals(leaf, ((Internal) result.getNode('f')).getNode('s'));
//		logger.info("outer: " + outer);
//		logger.info("internal3: " + result);
//		try{
//			assertEquals(outer, ((Internal) result.getNode('f')).getNode('e'));
//		} catch (Error e) {
//			Root.getInstance().print();
//		}
//	}
//
//	@Test
//	public void insertNode() throws ParentOfRootRequestedException, EndOfLeafException, ChildNotFoundException {
//		activePoint = new ActivePoint(internal3, 3);
//		tree = new SuffixTree();
//
//		Internal result = (Internal) tree.insertNode(internal3, TRACKID, 4, 1, null);
//		Internal inner = new Internal(6, 9, result, TRACKID);
//		Leaf leaf = new Leaf(4, 11, inner, TRACKID);
//
//		assertEquals(internal3, result);
//		assertEquals(leaf, result.getNode('s'));
//	}
//
//	@Test
//	public void insertNode_toRoot() throws ParentOfRootRequestedException, EndOfLeafException, ChildNotFoundException {
//		tree = new SuffixTree();
//
//		Internal result = (Internal) tree.insertNode(tree.root, TRACKID, 0, 1, null);
//		Leaf leaf = new Leaf(0, 11, tree.root, TRACKID);
//
//		assertEquals(leaf, result.getNode('a'));
//	}
//}
