package com.tracklookup.suffixtree;

import static org.junit.Assert.*;

import com.tracklookup.Application;
import com.tracklookup.ChannelTable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class LeafTest {
	final static private int END = 8;

	@Autowired
	ChannelTable channelTable;

	@Autowired
	Root root;

	@Test
	public void test() {
		Leaf leaf = new Leaf(1, root, 3, END, root, 8);
		assertEquals(5, leaf.getLength());
	}

}
