//package com.tracklookup.utility;
//
//
//import static org.junit.Assert.assertEquals;
//
//import java.util.List;
//
//import org.junit.Test;
//
//import com.tracklookup.Song;
//import com.tracklookup.Track;
//
//public class DAOTest {
//	final private String NOTES = "AFEASEDHTFAOIEJF";
//	final private String ARTIST = "Best Band";
//	final private String TITLE = "Biggist Hit";
//	TrackDAO trackDAO = new TrackDAO();
//	SongDAO songDAO = new SongDAO();
//
//	@Test
//	public void testTrackDAO() {
//		Track track = new Track(34, NOTES);
//		trackDAO.saveTrack(track);
//		List<Track> tracks = trackDAO.getAllTrack();
//
//		int id = 0;
//		for(Track t: tracks) {
//			if(t.getNotes().equals(NOTES)) {
//				id = t.getId();
//			}
//		}
//
//		Track T2 = trackDAO.getTrack(id);
//
//		assertEquals(NOTES, T2.getNotes());
//	}
//
//	@Test
//	public void testSongDAO() {
//		Song song = new Song(ARTIST, TITLE);
//		songDAO.saveSong(song);
//		List<Song> songs = songDAO.getAllSong();
//
//		int id = 0;
//		for(Song s: songs) {
//			if(s.getTitle().equals(TITLE)) {
//				id = s.getId();
//			}
//		}
//
//		Song S2 = songDAO.getSong(id);
//
//		assertEquals(TITLE, S2.getTitle());
//	}
//}