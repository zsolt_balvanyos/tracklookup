package com.tracklookup;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.ShortMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration(classes = Application.class)
public class SongAnalyserTest extends SongAnalyser {
	ShortMessage sm1;
	ShortMessage sm2;
	ShortMessage sm3;
	ShortMessage sm4;
	ShortMessage sm5;

	@Autowired
	ChordTable table;

	@Autowired
	SongAnalyser songAnalyser;
	
	@Before
	public void setup() throws InvalidMidiDataException {
		sm1 = new ShortMessage(ShortMessage.NOTE_ON, 1, 56, 0);
		sm2 = new ShortMessage(ShortMessage.NOTE_ON, 1, 54, 0);
		sm3 = new ShortMessage(ShortMessage.NOTE_ON, 1, 48, 0);
		sm4 = new ShortMessage(ShortMessage.NOTE_ON, 1, 51, 0);
		sm5 = new ShortMessage(ShortMessage.NOTE_ON, 1, 49, 0);
		songAnalyser.setChordRange(500);
	}
	
	@Test
	public void translateNotes() {
		List<Integer> notes = Arrays.asList(0, 2, 6);
		String output = songAnalyser.translateNotes(notes);
		String expected = "CE";
		assertEquals(expected, output);
	}
	
	@Test
	public void getRoot() {
		Set<Integer> chords = new HashSet<Integer>();
		chords.add(6);
		chords.add(0);
		chords.add(3);

		assertEquals(0, table.getRoot(chords));
	}
	
	@Test
	public void getRoot_2() {
		Set<Integer> chords = new HashSet<Integer>();
		chords.add(8);
		chords.add(2);
		chords.add(5);
		
		assertEquals(2, table.getRoot(chords));
	}
	
	@Test
	public void getRoot_1() {
		Set<Integer> chords = new HashSet<Integer>();
		chords.add(8);
		
		assertEquals(8, table.getRoot(chords));
	}
	
	@Test
	public void analyseChannel_NCN() {
		List<MidiEventShort> messages = new ArrayList<MidiEventShort>();
		messages.add(new MidiEventShort(sm1, 10254));
		messages.add(new MidiEventShort(sm2, 10791));
		messages.add(new MidiEventShort(sm3, 10854));
		messages.add(new MidiEventShort(sm4, 10878));
		messages.add(new MidiEventShort(sm5, 11471));
		String output = songAnalyser.analyseChannel(messages);
		String expected = "IB";
		
		assertEquals(expected, output);
	}
	
	@Test
	public void analyseChannel_NNC() {
		List<MidiEventShort> messages = new ArrayList<MidiEventShort>();
		messages.add(new MidiEventShort(sm1, 7254));
		messages.add(new MidiEventShort(sm2, 10291));
		messages.add(new MidiEventShort(sm3, 10854));
		messages.add(new MidiEventShort(sm4, 10878));
		messages.add(new MidiEventShort(sm1, 10971));
		String output = songAnalyser.analyseChannel(messages);
		String expected = "CC";
		
		assertEquals(expected, output);
	}

	//@Test
	public void underTheBridge() throws InvalidMidiDataException, IOException {
		File utb = new File("D:\\Project\\MIDI samples\\red_hot_chili_peppers-under_the_bridge.mid");
		songAnalyser.analyseMidiFile(utb);
	}

	@Test
	public void translate() {
		List<Integer> notes = new ArrayList<>(Arrays.asList(65,72,82,87,98,106,117,127,130,136,142,144,154,157,168,171,175));
		assertEquals("HKFLILKDGGCKDLDE", songAnalyser.translateNotes(notes));
	}
}
