DROP TABLE IF EXISTS Track;
DROP TABLE IF EXISTS Song;

CREATE TABLE Song
(
songid int NOT NULL AUTO_INCREMENT,
artist varchar(255),
title varchar(255),
PRIMARY KEY (songid)
);

CREATE TABLE Track
(
trackid int NOT NULL AUTO_INCREMENT,
songid int,
notes longtext,
PRIMARY KEY (trackid),
FOREIGN KEY (songid) REFERENCES Song(songid)
);

