var controller;
var song = ""; 
var recording = false;
var vInput;
var inputs;
var sessionId;
var notes = [];
var MINIMUM_NOTES = 4;

function record() {
	if(!recording) {
		document.getElementById("record").className = "onRec";
		recording = true;
	} else {
		document.getElementById("record").className = "offRec";
		recording = false;
	}
}

function device() {
	var vDevs = document.getElementById("midiController");
	var vDev = vDevs.options[vDevs.selectedIndex].value;
	
	for (let input of inputs.values()) {			
		if(vDev == input.name) {
			input.onmidimessage = analyseMessage;
		}
	}
}
	
if (navigator.requestMIDIAccess) {
	navigator.requestMIDIAccess({sysex: false}).then(onMIDISuccess, onMIDIFailure);
} else {
	alert("MIDI is not supported by your browser.");
}

function onMIDISuccess(midiAccess) {			
	var input = midiAccess.inputs.values();
	inputs = midiAccess.inputs;
	
	var opt = document.createElement("option");
	opt.text =  "Select your device";
	document.getElementById("midiController").add(opt);
		
	for (let input of inputs.values()) {
		var opt = document.createElement("option");
		opt.text = input.name;
		document.getElementById("midiController").add(opt);
	}
}

function analyseMessage(message) {
    var command = message.data[0];
    var note = message.data[1];
    var NOTE_ON = 144;

	if(command == NOTE_ON && recording) {
		song += " " + note;
		document.getElementById("notes").innerHTML = song;

		notes.push(note);
		if(notes.length > MINIMUM_NOTES) {
			sendQuery()
		}
	}
}		

function sendQuery() {
	var req = new XMLHttpRequest();
	req.open("GET", "analyser/" + notes) ;
	req.send();
	
	var results = [];
	req.onreadystatechange = function () {
		if(req.readyState == 4 && req.status == 200) {
			results = JSON.parse(req.responseText);
			var resList = "<ul>";
			for (i = 0; i < results.length; i++) {
				resList += "<li>" + results[i].artist + " - " + results[i].title + "</li>";
			}
			resList += "</ul>";
			document.getElementById("results").innerHTML = resList;
		}
	};
}

function test() {
	testInput = document.getElementById("input").value + '';
	notes = testInput.split(',');
	console.log(notes);
	sendQuery();
}

function onMIDIFailure(e) {
	console.log("No access to MIDI devices or your browser doesn't support WebMIDI API. Please use WebMIDIAPIShim " + e);
}