package com.tracklookup.suffixtree;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ChildNotFoundException;

public class ActivePoint {
	private Node activeNode;
	private int depth;
	private ChannelTable channelTable;
	private Logger logger; 

	
	public ActivePoint(Node activeNode, int depth, ChannelTable channelTable) {
		this.activeNode = activeNode;
		this.depth = depth;
		this.channelTable = channelTable;
		this.logger = LogManager.getLogger(ActivePoint.class);
	}
	
	public boolean hasChar(char c) {
		if(activeNode.getLength() <= depth) {
			if(activeNode instanceof Leaf) {
				return false;
			} else {
				try {
					setActiveNode(((Internal) activeNode).getNode(c));
					setDepth(0);
					return true;
				} catch (ChildNotFoundException e) {
					return false;
				}
			}
		} else {
			int position = activeNode.getStart() + depth;
			String notes = channelTable.getChannel(activeNode.channelId).getNotes();
			if(notes.charAt(position) == c) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public Node getActiveNode() {
		return activeNode;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public boolean incrementDepth() {
		if(logger.isDebugEnabled()) logger.debug("Depth changes from " + depth + " to " + (depth + 1));
		depth++;
		
		if(depth == activeNode.length) {
			return true;
		} else {
			return false;
		}
	}

	private void setActiveNode(Node activeNode) {
		if(logger.isDebugEnabled()) logger.debug("ActiveNode changes from [" + this.activeNode + "] to [" + activeNode + "]");
		this.activeNode = activeNode;
	}

	private void setDepth(int depth) {
		if(logger.isDebugEnabled()) logger.debug("Depth changes from " + this.depth + " to " + depth);
		this.depth = depth;
	}
	
	public String toString() {
		return "node: " + activeNode + ", depth: " + depth ;
	}
}