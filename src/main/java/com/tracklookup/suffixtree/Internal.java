package com.tracklookup.suffixtree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ChildNotFoundException;

public class Internal extends Node {
	protected Map <Character, Node> nodes;
	private Internal link;
	final private String DELIMITER = "#";
	Logger logger;
	
	public Internal(int id, Root root, int start, int end, Internal parent, int channelId, Map <Character, Node> nodes, Set<Integer> channels) {
		super(id, root, start, end, channelId, parent, channels);
		this.nodes = nodes;
		this.link = root;
		logger = LogManager.getLogger(Internal.class);
		if(logger.isDebugEnabled()) logger.debug("\tInternal created: " + this + "[" + hashCode() + "]");
	}
	
	public Internal(int id, Root root, int start, int end, Internal parent, int channelId) {
		this(id, root, start, end, parent, channelId, new HashMap<Character, Node>(), new HashSet<Integer>());
	}
	
	public Internal getLink() {
		return link;
	}
	
	public void setLink(Internal node) {
		this.link = node;
	}
	
	public Node getNode(char c) throws ChildNotFoundException {
		if(nodes.containsKey(c)) return nodes.get(c);
		throw new ChildNotFoundException(this, c);
	}
	
	public void setNode(char c, Node node) {
		nodes.put(c, node);
	}
	
	public Map <Character, Node> getNodes() {
		return nodes;
	}

	@Override
	public String toString() {
		String nodz = "";
		for(Map.Entry<Character, Node> e: nodes.entrySet()) {
			nodz += "," + e.getKey() + e.getValue().id;
		}
		return "I" + DELIMITER + super.toString() + DELIMITER + nodz + DELIMITER + link.id + DELIMITER;
	}

	@Override
	public void print(ChannelTable channelTable, int level) {
		String text = "";
		for(int i = 0; i < level; i++) {
			text += "\t|";
		}
		text += getString(channelTable);
		logger.warn(text);
		for(Character c: nodes.keySet()) {
			nodes.get(c).print(channelTable, level + 1);
		}
	}
	
	@Override
	public String testPrint(ChannelTable channelTable, int level) {
		String text = "";
		for(int i = 0; i < level; i++) {
			text += "\t|";
		}
		text = text + getString(channelTable) + "\n";
		for(Character c: nodes.keySet()) {
			text += nodes.get(c).testPrint(channelTable, level + 1);
		}
		return text;
	}

	private String getString(ChannelTable channelTable) {
		String channel = channelTable.getChannel(channelId).getNotes();
		return channel.substring(start, start + getLength()) + " [" + id + "] L:[" + link.id + "] P:[" + parent.id + "]";
	}
}
