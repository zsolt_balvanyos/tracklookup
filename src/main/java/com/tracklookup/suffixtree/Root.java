package com.tracklookup.suffixtree;

import org.springframework.stereotype.Component;

import com.tracklookup.exceptions.ParentOfRootRequestedException;

import java.util.Map;

@Component
public class Root extends Internal {
	String DELIMITER = "#";

	private Root() {
		super(0, null, 0, 0 , null, 0);
	}
	
	@Override
	public int getLength() {
		return 0;
	}
	
	@Override
	public Internal getLink() {
		return this;
	}
	
	@Override
	public int getChannelId() {
		return 0;
	}

	@Override
	public String toString() {
		String nodz = "";
		for(Map.Entry<Character, Node> e: nodes.entrySet()) {
			nodz += "," + e.getKey() + e.getValue().id;
		}
		return "R" + DELIMITER + nodz + DELIMITER;
	}

	@Override
	public Internal getParent() throws ParentOfRootRequestedException{
		throw new ParentOfRootRequestedException();
	}
}
