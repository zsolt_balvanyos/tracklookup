package com.tracklookup.suffixtree;

import java.util.Set;

import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ParentOfRootRequestedException;

public abstract class Node {
	final protected int id;
	final protected int channelId;
	final protected int start;
	final protected int length;
	final protected Set<Integer> channels;
	final protected Root root;
	protected Node parent;
	final private String DELIMITER = "#";

	public Node(int id, Root root, int start, int end, int channelId, Internal parent, Set<Integer> channels) {
		this.id = id;
		this.start = start;
		this.length = end - start;
		this.channelId = channelId;
		this.parent = parent;
		this.channels = channels;
		channels.add(channelId);
		this.root = root;
	}
	
	public void registerChannelIdFromRoot(int channelId) {
		Node node = this;		
		while(node != root) {
			node.channels.add(channelId);
			node = node.parent;
		}
	}

	public String toString() {
		String chans = "";
		for(Integer i: channels) {
			chans += "," + i;
		}

		return start + DELIMITER + (start + length) + DELIMITER + channelId + DELIMITER + parent.id + DELIMITER + chans.substring(1) + DELIMITER;
	}

	public int getId() {
		return id;
	}

	public void addID(int channelId) {
		channels.add(channelId);
	}
	
	public Set<Integer> getChannels() {
		return channels;
	}

	public int getChannelId() {
		return channelId;
	}
	
	public Internal getParent() throws ParentOfRootRequestedException {
		return (Internal) parent;
	}
	
	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	public int getStart() {
		return start;
	}
	
	public int getLength() { 
		return length; 
	}

	public void print(ChannelTable channelTable) {
		print(channelTable, -1);
	}

	public String testPrint(ChannelTable channelTable) {
		return testPrint(channelTable, -1);
	}

	public abstract void print(ChannelTable channelTable, int level);
	
	public abstract String testPrint(ChannelTable channelTable, int level);
}
