package com.tracklookup.suffixtree;

import java.util.*;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tracklookup.ChannelTable;
import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.EndOfLeafException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import com.tracklookup.utility.NodeFactory;
import com.tracklookup.utility.Factory;

@Component
public class SuffixTree {
	Logger logger = LogManager.getLogger(SuffixTree.class);
	public final static char TERMINAL_CHAR = '$';
	private final String phaseSeparator = "\n================================================================================================================";
	private final String extensionSeparator = "....................................................................";

	@Autowired
	ChannelTable channelTable;

	@Autowired
	Root root;

	@Autowired
	Factory factory;

	@Autowired
	NodeFactory nodeFactory;

	public SuffixTree() {
		logger.setLevel(Level.INFO);
	}
	
	public void addChannel(int channelId) throws ChildNotFoundException, ParentOfRootRequestedException, EndOfLeafException {
		System.out.println("Adding channel " + channelId);
		ActivePoint activePoint = factory.getActivePoint(root, 0);

		/*
		 * Appending the terminal character
		 */
		String text = channelTable.getChannel(channelId).getNotes() + TERMINAL_CHAR;
		channelTable.setChannel(channelId, text);
		int j = 0;

		for(int i = 0; i < text.length(); i++) {
			if(logger.isDebugEnabled()) {
				logger.debug(phaseSeparator);
				logger.debug("ActivePoint: " + activePoint + ", j: " + j + ", i: " + i + " Text: " + text.substring(0, i + 1) );
			}
			char c = text.charAt(i);

			if(!activePoint.hasChar(c)) {
				Internal inner = insertNode(activePoint.getActiveNode(), channelId, i, activePoint.getDepth(), null);
				j++;

				while(j <= i) {
					if(logger.isDebugEnabled()) {
						logger.debug(extensionSeparator);
						logger.debug("Inner: " + inner + ", j: " + j + ", i: " + i );
					}
					
					int distance;
					Node walkStartNode;

					if(inner.getParent().getLink().equals(root)) {
						if(logger.isDebugEnabled()) logger.debug("Distance from root = " + i + " - " + j + " = " + (i - j));
						distance = i - j;
						if(j == i) {
							walkStartNode = root;
						} else {
							walkStartNode = root.getNode(text.charAt(j));
						}
					} else {
						if(logger.isDebugEnabled()) logger.debug("Distance from internal = " + inner.getLength());
					 	distance = inner.getLength();
					 	walkStartNode = inner.getParent().getLink().getNode(getStartChar(inner));
					}

					activePoint = walkDown(walkStartNode, distance, channelId, i);

					/*
					 * Adding the channel id to each nodes on the path from the root to the new active node
					 */
					activePoint.getActiveNode().registerChannelIdFromRoot(channelId);

					if(activePoint.hasChar(c)) {
						boolean endOfNodeReached = activePoint.incrementDepth();
						if(endOfNodeReached) activePoint.getActiveNode().addID(channelId);
						break;
					}

					inner = insertNode(activePoint.getActiveNode(), channelId, i, activePoint.getDepth(), inner);
					j++;
				}
			} else {
				boolean endOfNodeReached = activePoint.incrementDepth();
				if(endOfNodeReached) activePoint.getActiveNode().addID(channelId);
			}
		}
		
		for(Node node: NodeFactory.tempNodes) {
			node.getParent().getNodes().remove(TERMINAL_CHAR);
		}

		NodeFactory.tempNodes.clear();
		channelTable.setChannel(channelId, text.substring(0, text.length() - 1));
	}

	public ActivePoint walkDown(Node node, int distance, int channelId, int i) throws ChildNotFoundException, EndOfLeafException, ParentOfRootRequestedException {
		node.channels.add(channelId);
		
		if(distance <= node.getLength()) {
			return factory.getActivePoint(node, distance);
		}
		
		if(node instanceof Leaf) {
			if(channelTable.getChannel(channelId).getNotes().charAt(i) == TERMINAL_CHAR) {
				return factory.getActivePoint(node, distance);
			}
			throw new EndOfLeafException(node.getChannelId(), node.getStart());
		} 
		
		int nextNodeStartPosition = i - distance + node.getLength();
		Node nextNode = ((Internal) node).getNode(channelTable.getChannel(channelId).getNotes().charAt(nextNodeStartPosition));
	
		return walkDown(nextNode, distance - node.getLength(), channelId, i);
	}

	public Internal insertNode(Node node, int channelId, int i, int depth, Internal lastSplitNode) throws ParentOfRootRequestedException, EndOfLeafException {
		if(logger.isDebugEnabled()) {
			logger.debug("\n\tInsert called with depth " + depth + " node length " + node.getLength());
		}

		if(node.getLength() == depth) {
			Internal internal;
			
			if(node instanceof Leaf) {
				Internal parent = node.getParent();
				internal = nodeFactory.getInternal(node.getStart(), node.getStart() + node.getLength(), parent, node.getChannelId());
				parent.setNode(getStartChar(internal), internal);
			} else {
				internal = (Internal) node;
			}
			
			Leaf leaf = nodeFactory.getLeaf(i, channelTable.getChannel(channelId).getNotes().length() - 1, internal, channelId);
			internal.setNode(getStartChar(leaf), leaf);

			
			if(logger.isDebugEnabled()) {
				logger.debug("\tReturning " + internal + " from insertNode\n");
			}
			return internal;
			
		} else {	
			return split(node, channelId, i, depth, lastSplitNode);
		}
	}

	public Internal split(Node node, int channelId, int i, int depth, Internal lastSplitNode) throws ParentOfRootRequestedException {

		if(logger.isDebugEnabled()) {
			logger.debug("\n\tSplitting " + node + ", its parent is " + node.getParent());
		}

		int oldChannelId = node.getChannelId();
		Set<Integer> oldChannels = node.getChannels();


		/*
		 * Create the channelID set for the inner node that has all the old channels and the new one
		 */
		Set<Integer> newChannels = new HashSet<Integer>();
		newChannels.addAll(oldChannels);
		newChannels.add(channelId);


		/*
		 * Create the nodes needed
		 */
		Internal parent = node.getParent();
		Internal inner = nodeFactory.getInternal(node.getStart(), node.getStart() + depth, parent, oldChannelId, newChannels);
		Node leaf = nodeFactory.getLeaf(i, channelTable.getChannel(channelId).getNotes().length() - 1, inner, channelId);
		Node outer;


		/*
		 * If the node that we are splitting is an internal node, the outer has to be internal as well.
		 * As the node is internal, it already has children, the parent of these children has to be
		 * reset to the new outer node.
		 */
		if(node instanceof Internal) {
			Map <Character, Node> nodes = ((Internal) node).getNodes();
			outer = nodeFactory.getInternal(node.getStart() + depth, node.getStart() + node.getLength(), inner, oldChannelId, nodes, oldChannels);
			for(Node n: nodes.values()) {
				n.setParent(outer);
			}
		} else {
			outer = nodeFactory.getLeaf(node.getStart() + depth, channelTable.getChannel(oldChannelId).getNotes().length() - 1, inner, oldChannelId);
		}


		inner.setNode(getStartChar(outer), outer);
		inner.setNode(getStartChar(leaf), leaf);
		parent.setNode(getStartChar(inner), inner);

		if(lastSplitNode != null) {
			lastSplitNode.setLink(inner);
		}

		if(logger.isDebugEnabled()) {
			logger.debug("\tSplit returns: " + inner);
		}
		return inner;
	}

	public char getStartChar(Node node) {
		try{
			return channelTable.getChannel(node.getChannelId()).getNotes().charAt(node.getStart());
		} catch (IndexOutOfBoundsException e) {
			return TERMINAL_CHAR;
		}

	}

	public Root getRoot() {
		return root;
	}

	public NodeFactory getNodeFactory() {
		return nodeFactory;
	}
}
