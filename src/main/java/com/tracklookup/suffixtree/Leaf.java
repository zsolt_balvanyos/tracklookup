package com.tracklookup.suffixtree;

import java.util.HashSet;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.tracklookup.ChannelTable;

public class Leaf extends Node {
	Logger logger;
	final private String DELIMITER = "#";

	public Leaf(int id, Root root, int start, int end, Internal parent, int channelId) {
		super(id, root, start, end, channelId, parent, new HashSet<Integer>());
		logger = LogManager.getLogger(Leaf.class);
		if(logger.isDebugEnabled()) logger.debug("\tLeaf created: " + this + "[start: " + start + ", end: " + end + "]");
	}

	@Override
	public String toString() {
		return "L" + DELIMITER + super.toString();
	}

	@Override
	public void print(ChannelTable channelTable, int level) {
		String text = "";
		for(int i = 0; i < level; i++) {
			text = text + "\t|";
		}
		logger.warn(text + getString(channelTable));
	}
	
	@Override
	public String testPrint(ChannelTable channelTable, int level) {
		String text = "";
		for(int i = 0; i < level; i++) {
			text = text + "\t|";
		}
		return text + getString(channelTable) + "\n";
	}

	private String getString(ChannelTable channelTable) {
		String channel = channelTable.getChannel(channelId).getNotes();
		return channel.substring(start, start + getLength()) + " [" + id + "] P:[" + parent.id + "]";
	}
}
