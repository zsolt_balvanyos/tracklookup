package com.tracklookup.web;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tracklookup.Song;

@RestController
public class Controller {
	Logger logger = LogManager.getLogger(Controller.class);
	
	@Autowired
	UserServices userServices;
	
	@Autowired
	AdminService adminService;


	
	@RequestMapping(value="/analyser/{notes}", 
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<Song>> evaluate(@PathVariable List<Integer> notes) {

		Set<Song> songs = userServices.evaluate(notes);

		return new ResponseEntity<Set<Song>>(songs, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/upload", 
					method = RequestMethod.POST, 
					produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<String> addFile(	@RequestParam("file") MultipartFile file,
											@RequestParam("artist") String artist,
											@RequestParam("title") String title) {

		String response = adminService.addSong(file, artist, title);

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
}