package com.tracklookup.web;

import com.tracklookup.*;
import com.tracklookup.exceptions.ChildNotFoundException;
import com.tracklookup.exceptions.EndOfLeafException;
import com.tracklookup.exceptions.ParentOfRootRequestedException;
import com.tracklookup.suffixtree.SuffixTree;

import com.tracklookup.utility.QueryPerformanceTester;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 * Created by zbalvanyos on 31/07/2016.
 */

@Service
public class AdminService {
	final Logger logger = LogManager.getLogger(AdminService.class);

    @Autowired
    SuffixTree suffixTree;

    @Autowired
    ChannelTable channelTable;

    @Autowired
    SongTable songTable;

    @Autowired
    SongAnalyser songAnalyser;

    @Autowired
    QueryPerformanceTester querySpeed;

    @PostConstruct
	private void init() {
        int marker = 0;
        int totalCharacters = 0;
        int sampleFrequency = 5000;
		long accumulatedTime = 0;
        boolean performanceTesting = true;
        Runtime runTime = Runtime.getRuntime();

    	for(int channelId: channelTable.getMap().keySet()) {
			try {
                long startTime = System.currentTimeMillis();

                suffixTree.addChannel(channelId);

                if(performanceTesting) {
                    totalCharacters += channelTable.getMap().get(channelId).getNotes().length();
                    if (totalCharacters >= marker) {
                        accumulatedTime += System.currentTimeMillis() - startTime;
                        runTime.gc();
                        long space = runTime.totalMemory() - runTime.freeMemory();
//                        logger.info(totalCharacters + "," + accumulatedTime + "," + space + "," + querySpeed.measureQuerySpeed() + "," + suffixTree.getNodeFactory().getCounter());
                        logger.info(totalCharacters + "," + accumulatedTime + "," + space + "," + suffixTree.getNodeFactory().getCounter());
                        marker += sampleFrequency;
                    }
                }
			} catch (ChildNotFoundException | ParentOfRootRequestedException | EndOfLeafException e) {
				logger.error(e);
			}
    	}
    }
    
    public String addSong(MultipartFile file, String artist, String title) {
        try {
            if(!file.isEmpty()) {
                Path location = Paths.get(Application.FILE_LOCATION, file.getOriginalFilename());

                Files.copy(file.getInputStream(), location);

                File savedFile = location.toFile();
                String response = addSong(savedFile, artist, title);
                savedFile.delete();
                return response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Upload failed because the file is empty";
    }

    public String addSong(File file, String artist, String title) {
        String response = "";

        try {
            int songId = songTable.saveSong(artist, title);

            List<String> channels = songAnalyser.analyseMidiFile(file);

            for(String notes: channels) {
                int channelId = channelTable.saveChannel(songId, notes);
                suffixTree.addChannel(channelId);
                response = "Successfully uploaded!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            response = "Upload failed, " + e.getMessage();
        }
        return response;
    }
}
