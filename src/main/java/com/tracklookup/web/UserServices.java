package com.tracklookup.web;

import java.util.*;

import com.tracklookup.*;
import com.tracklookup.suffixtree.Root;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tracklookup.suffixtree.ActivePoint;
import com.tracklookup.utility.Factory;

@Service
public class UserServices {
	Logger logger = LogManager.getLogger(UserServices.class);
	final int DISPLAYED_RESULTS = 5;
	final int TOP_SCORES = 5;

	@Autowired
	ChannelTable channelTable;
	
	@Autowired
	SongTable songTable;

	@Autowired
	SongAnalyser songAnalyser;

	@Autowired
	Factory factory;

	@Autowired
	Root root;
	
	
	public LinkedHashSet<Song> evaluate(List<Integer> notes) {
		Map<Integer, Integer> evaluator = new HashMap<Integer, Integer>();
		Set<Integer> channelIds = channelTable.getMap().keySet();
		
		for(int channelId: channelIds) {
			evaluator.put(channelId, 0);
		}
		
		/*
		 * Evaluate each 'suffix' of the list of notes.
		 */
		String translatedNotes = songAnalyser.translateNotes(notes);
		System.out.println("Translated query: " + translatedNotes);

		for(int i = 0; i < translatedNotes.length(); i++) {
			Map<Integer, Integer> subString = evaluateHelper(translatedNotes.substring(i));
			
			for(int channelId: channelIds) {
				int oldScore = evaluator.get(channelId);
				int newScore = oldScore + subString.get(channelId);
				evaluator.put(channelId, newScore);
			}
		}
		
		/*
		 * Sort the evaluator by value
		 */
		List<Integer> scores = new ArrayList<>();
		scores.addAll(evaluator.values());
		Collections.sort(scores);
		Collections.reverse(scores);
		Set<Integer> topOnes = new HashSet<>();
		for(int i = 0; i < TOP_SCORES; i++) {
			topOnes.add(scores.get(i));
		}

		/*
		 * Those channels that have the maximum scores are collected.
		 */
		List<Winner> winners = new ArrayList<Winner>();
		for(int channelId: channelIds) {
			int score = evaluator.get(channelId);
			if(topOnes.contains(score)) {
				Winner winner = new Winner(channelId, score);
				System.out.println(winner);
				winners.add(winner);
			}
		}
		System.out.println(".....................");

		Collections.sort(winners);

		LinkedHashSet<Song> result = new LinkedHashSet<>();
		int i = 0;
		
		while(i < winners.size() && i < DISPLAYED_RESULTS) {
			Channel channel = channelTable.getChannel(winners.get(i).channelId);
			int songId = channel.getSongId();
			result.add(songTable.getSong(songId));
			i++;
		}

		return result;
	}
	
	private Map<Integer, Integer> evaluateHelper(String notes) {
		Set<Integer> channelIds = channelTable.getMap().keySet();
		ActivePoint activePoint = factory.getActivePoint(root, 0);
		Map<Integer, Integer> evaluator = new HashMap<Integer, Integer>();
		
		for(int channelId: channelIds) {
			evaluator.put(channelId, 0);
		}
		
		int index = 0;
		
		/*
		 * For every channelId that belongs to the position in the tree where the activePoint 
		 * is pointing currently increment the scores by 1.
		 */
		while(index < notes.length() && activePoint.hasChar(notes.charAt(index))) {
			for(int channel: activePoint.getActiveNode().getChannels()) {
				int score = evaluator.get(channel) + 1;
				evaluator.put(channel, score);
			}
			
			activePoint.incrementDepth();
			index++;
		}
		
		return evaluator;
	}

	private class Winner implements Comparable {
		int channelId;
		int score;

		public Winner(int channelId, int score) {
			this.channelId = channelId;
			this.score = score;
		}

		@Override
		public int compareTo(Object o) {
			Winner other = (Winner) o;
			if(score == other.score) return channelId - other.channelId;
			return other.score - score;
		}

		@Override
		public String toString() {
			return channelId + " - " + score;
		}
	}
}