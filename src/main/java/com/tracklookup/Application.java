package com.tracklookup;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sound.midi.InvalidMidiDataException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class Application {
	final static public String FILE_LOCATION = System.getProperty("user.home") + File.separator + "TrackLookup";

	public static void main(String[] args) {
		Path location = Paths.get(FILE_LOCATION);
		if(!Files.exists(location)) {
			new File(FILE_LOCATION).mkdir();
		}
		SpringApplication.run(Application.class, args);

	}
}