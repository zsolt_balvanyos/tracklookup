package com.tracklookup;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SongAnalyser {
	final private int A = 65;
	final private int TWO_NOTES = 2;
	final private int NUMBER_OF_CHANNELS = 16;
	private long chordRange = 10;
	private int NOTES_IN_OCTAVE = 12;
	Logger logger = LogManager.getLogger(SongAnalyser.class);

	@Autowired
	ChordTable chordTable;

	public List<String> analyseMidiFile(File file) throws InvalidMidiDataException, IOException {
		System.out.println("Analysing file: " + file.getName());
		List<String> result = new ArrayList<String>();
		
		/*
		 * For each of the 16 MIDI channels we need a MidiEvent list to contain the notes.
		 */
		@SuppressWarnings("unchecked")
		List<MidiEventShort>[] channels = new ArrayList[NUMBER_OF_CHANNELS];
		for(int i = 0; i < NUMBER_OF_CHANNELS; i++) {
			channels[i] = new ArrayList<MidiEventShort>();
		}
		
		/*
		 * Go through each message of each track and separate the by channel
		 */
		int tid = 1;
		Sequence sequence = MidiSystem.getSequence(file);
		for(Track track: sequence.getTracks()) {
			for(int i = 0; i < track.size(); i++) {
				MidiEvent event = track.get(i);
				MidiMessage message = event.getMessage();
				
				if(message instanceof ShortMessage) {
					ShortMessage sm = (ShortMessage) message;
					if(sm.getCommand() == ShortMessage.NOTE_ON && sm.getData2() > 0) {
						
						/*
						 * Create a new MidiEventShort instance and add it to the channel it belongs to.
						 */
						int channelNo = sm.getChannel();
						//logger.error(sm.getData1() + " " + sm.getData2());
						MidiEventShort shortEvent = new MidiEventShort(sm ,event.getTick());
						channels[channelNo].add(shortEvent);
					}
				}
			}		
		}

		for(List<MidiEventShort> channel: channels) {
			if(channel.size() > TWO_NOTES) {
//				logger.error(channel);
				result.add(analyseChannel(channel));
			}
		}
		
		return result;
	}
	
	
	protected String analyseChannel(List<MidiEventShort> channel) {
		List<Integer> analysedChannel = new ArrayList<Integer>();
		Set<Integer> chord = new HashSet<Integer>();
		int eventNo = 1;
		chord.add(channel.get(0).NOTE);
		
		while(eventNo < channel.size()) {
			MidiEventShort event = channel.get(eventNo);

			if(channel.get(eventNo).TICK - channel.get(eventNo - 1).TICK > chordRange) {
				addChord(analysedChannel,chord);
				chord = new HashSet<Integer>();	
			}
			chord.add(event.NOTE);
			eventNo++;
		}
		addChord(analysedChannel,chord);

		return translateNotes(analysedChannel);
	}

	public String translateNotes(List<Integer> input) {
		String translatedChannel = "";

		List<Integer> notes = input.stream()
				.map(note -> note % NOTES_IN_OCTAVE)
				.collect(Collectors.toList());

		for(int i = 1; i < notes.size(); i++) {
			int distance = Math.abs(notes.get(i - 1) - notes.get(i));
			translatedChannel += (char) (A + distance);
		}
		return translatedChannel;
	}

	private void addChord(List<Integer> analysedChannel, Set<Integer> chord) {
		int root = chordTable.getRoot(chord);
		if(root == 13) {
			analysedChannel.addAll(chord);
		} else {
			analysedChannel.add(root);
		}
	}
	
	public long getChordRange() {
		return chordRange;
	}
	
	public void setChordRange(long chordRange) {
		this.chordRange = chordRange;
	}
}
