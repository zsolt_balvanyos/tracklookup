package com.tracklookup;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import com.tracklookup.utility.Factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChannelTable {
	private Map<Integer, Channel> channels;
	private StringBuilder sb;
	private BufferedReader br;
	private BufferedWriter bw;
	private File channelsFile;
	private String fileName = "channels.txt";
	private String DELIMITER = "#";
	private int CHANNELID = 0;
	private int SONGID = 1;
	private int NOTES = 2;
	private AtomicInteger channelId = new AtomicInteger();

	@Autowired
	Factory factory;

    @PostConstruct
	public void init() throws IOException {

		channels = new HashMap<Integer, Channel>();
		channelsFile = new File(Application.FILE_LOCATION + File.separator + fileName);

		if(!channelsFile.exists()) {
			channelsFile.createNewFile();
		}

		sb = new StringBuilder();
		br = new BufferedReader(new FileReader(channelsFile));

		String line = "";
		while ((line = br.readLine()) != null) {
			String[] splitLine = line.split(DELIMITER);
			int id = Integer.parseInt(splitLine[CHANNELID]);
			int songId = Integer.parseInt(splitLine[SONGID]);
			String notes = splitLine[NOTES];
			Channel channel = factory.getChannel(id, songId, notes);

			channelId.set(id + 1);
			channels.put(channel.getId(), channel);
			sb.append(channel.getId() + DELIMITER + songId + DELIMITER + notes + System.lineSeparator());
		}
		
		br.close();
	}

	public int saveChannel(int songId, String notes) throws IOException {
		Channel channel = factory.getChannel(channelId.getAndIncrement(), songId, notes);
		channels.put(channel.getId(), channel);

		bw = new BufferedWriter(new FileWriter(channelsFile, true));
		bw.write(channel.getId() + DELIMITER + songId + DELIMITER + notes + System.lineSeparator());
		bw.close();

		return channel.getId();
	}

	public void setChannel(int channelId, String notes) {
		Channel channel = channels.get(channelId);
		channel.setNotes(notes);
	}

	public Channel getChannel(int channelId) {
		return channels.get(channelId);
	}

	public Map<Integer, Channel> getMap() {
		return channels;
	}
}
