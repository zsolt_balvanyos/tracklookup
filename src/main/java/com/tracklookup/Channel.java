package com.tracklookup;

public class Channel {

	private int channelId;
	
	private int songId;
	
	private String notes;
	
	public Channel() {}
	
	public Channel(int songId, String notes) {
		this.songId = songId;
		this.notes = notes;
	}	
	
	public Channel(int id, int songId, String notes) {
		this.channelId = id;
		this.songId = songId;
		this.notes = notes;
	}

	public int getId() {
		return channelId;
	}

	public int getSongId() {
		return songId;
	}

	public String getNotes() {
		return notes;
	}

	public void setId(int id) {
		this.channelId = id;
	}

	public void setSongId(int songId) {
		this.songId = songId;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}