package com.tracklookup;

public class Song {

	private int songId;
	
	private String artist;
	
	private String title;
	
	public Song() {}
	
	public Song(String artist, String title) {
		this.artist = artist;
		this.title = title;
	}

	public Song(int songId, String artist, String title) {
		this.songId = songId;
		this.artist = artist;
		this.title = title;
	}
	
	public int getId() {
		return songId;
	}

	public String getArtist() {
		return artist;
	}

	public String getTitle() {
		return title;
	}

	public void setSongId(int songId) {
		this.songId = songId;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return "[Artist: " + artist + " - Title: " + title + "]"; 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (artist == null) {
			if (other.artist != null)
				return false;
		} else if (!artist.equals(other.artist))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	
}