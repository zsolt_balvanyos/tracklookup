package com.tracklookup;

import com.tracklookup.utility.Factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

/**
 * Created by zbalvanyos on 31/07/2016.
 */

@Component
public class SongTable {
    private Map<Integer, Song> songs;
    private StringBuilder sb;
    private BufferedReader br;
    private BufferedWriter bw;
    private File songsFile;
    private String fileName = "songs.txt";
    private String DELIMITER = "#";
    private int SONGIDA = 0;
    private int ARTIST = 1;
    private int TITLE = 2;
    private AtomicInteger songId = new AtomicInteger();

    @Autowired
    Factory factory;

    @PostConstruct
    public void init() throws IOException {

        songs = new HashMap<Integer, Song>();
        songsFile = new File(Application.FILE_LOCATION + File.separator + fileName);

        if(!songsFile.exists()) {
            songsFile.createNewFile();
        }

        sb = new StringBuilder();
        br = new BufferedReader(new FileReader(songsFile));

        String line = "";

        while((line = br.readLine()) != null) {
            String[] splitLine = line.split(DELIMITER);
            int id = Integer.parseInt(splitLine[SONGIDA]);
            String artist = splitLine[ARTIST];
            String title = splitLine[TITLE];
            Song song = factory.getSong(id, artist, title);
            songId.set(id + 1);

            songs.put(song.getId(), song);
            sb.append(song.getId() + DELIMITER + artist + DELIMITER + title + System.lineSeparator());
        }
        
        br.close();
    }

    public Song getSong(int songId) {
        return songs.get(songId);
    }

    public int saveSong(String artist, String title) throws IOException {
        Song song = factory.getSong(songId.getAndIncrement(), artist, title);
        songs.put(song.getId(),song);

        bw = new BufferedWriter(new FileWriter(songsFile, true));
        bw.write(song.getId() + DELIMITER + artist + DELIMITER + title + System.lineSeparator());
        bw.close();

        return song.getId();
    }
}
