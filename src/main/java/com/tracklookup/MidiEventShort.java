package com.tracklookup;

import javax.sound.midi.ShortMessage;

/**
 * Created by Zsolt on 04-Sep-16.
 */
public class MidiEventShort {
    final int NOTE;
    final long TICK;

    public MidiEventShort(ShortMessage message, long tick) {
        this.NOTE = message.getData1();
        this.TICK = tick;
    }

    @Override
    public String toString() {
        return String.valueOf("[" + NOTE + "_" + TICK + "]");
    }
}
