package com.tracklookup.utility;

import com.tracklookup.Song;
import com.tracklookup.Channel;
import com.tracklookup.ChannelTable;
import com.tracklookup.suffixtree.ActivePoint;
import com.tracklookup.suffixtree.Node;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Factory {
	
	@Autowired
	ChannelTable channelTable;
	
	public ActivePoint getActivePoint(Node node, int distance) {
		return new ActivePoint(node, distance, channelTable);
	}

	public Song getSong(String artist, String title) {
		return new Song(artist, title);
	}

	public Song getSong(int songId, String artist, String title) {
		return new Song(songId, artist, title);
	}

	public Channel getChannel(int songId, String notes) {
		return new Channel(songId, notes);
	}

	public Channel getChannel(int channelId, int songId, String notes) {
		return new Channel(channelId, songId, notes);
	}
}