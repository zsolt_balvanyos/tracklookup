package com.tracklookup.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tracklookup.ChannelTable;
import com.tracklookup.suffixtree.Internal;
import com.tracklookup.suffixtree.Leaf;
import com.tracklookup.suffixtree.Node;
import com.tracklookup.suffixtree.Root;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NodeFactory {
	Logger logger = LogManager.getLogger(NodeFactory.class);
	public  static List<Node> tempNodes = new ArrayList<Node>();
	private  int counter = 0;

	@Autowired
	ChannelTable channelTable;

	@Autowired
	Root root;

	public int getCounter() {
		return counter;
	}
	
	public Leaf getLeaf(int start, int end, Internal parent, int channelId) {
		counter++;
		Leaf leaf = new Leaf(counter, root, start, end, parent, channelId);

		/*
		The terminal character $ has been appended to the end of each channel, this character can only occure at the last position.
		If the starting character equals the last character ($), we register this in tempNodes, these will be later removed.
		 */
		if(channelTable.getChannel(channelId).getNotes().length() - 1 == start) {
			tempNodes.add(leaf);
		}
		return leaf;
	}
	
	public Internal getInternal(int start, int end, Internal parent, int channelId) {
		counter++;
		return new Internal(counter, root, start, end, parent, channelId);
	}
	
	public Internal getInternal(int start, int end, Internal parent, int channelId, Map<Character, Node> nodes) {
		counter++;
		return new Internal(counter, root, start, end, parent, channelId, nodes, new HashSet<Integer>());
	}
	
	public Internal getInternal(int start, int end, Internal parent, int channelId, Set<Integer> channels) {
		counter++;
		return new Internal(counter, root, start, end, parent, channelId, new HashMap<Character, Node>(), channels);
	}
	
	public Internal getInternal(int start, int end, Internal parent, int channelId, Map<Character, Node> nodes, Set<Integer> channels) {
		counter++;
		return new Internal(counter, root, start, end, parent, channelId, nodes, channels);
	}
}
