package com.tracklookup.utility;

import com.tracklookup.Song;
import com.tracklookup.web.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Zsolt on 31-Aug-16.
 */
@Component
public class QueryPerformanceTester {
    @Autowired
    UserServices userServices;

    public long measureQuerySpeed() {
        long start = System.currentTimeMillis();
        userServices.evaluate(new ArrayList<>(Arrays.asList(65,74,81,85,90,95,100,111,120,125,128,138,143,146,152,154,158,159,167,175,178,179,189)));
        long duration = System.currentTimeMillis() - start;

        return duration;
    }
}
