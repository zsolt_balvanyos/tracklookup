package com.tracklookup.exceptions;

import com.tracklookup.suffixtree.Node;

@SuppressWarnings("serial")
public class ChildNotFoundException extends Exception {
	private Node node;
	private char childStartChar;
	
	public ChildNotFoundException(Node node, char childStartChar) {
		super();
		this.node = node;
		this.childStartChar = childStartChar;
	}
	
	public Node getNode() {
		return node;
	}
	
	public char getChildStartChar() {
		return childStartChar;
	}
	
	public String toString() {
		return "Node " + node + " has no child starting with " + childStartChar;
	}
}
