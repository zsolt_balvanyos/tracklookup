package com.tracklookup.exceptions;

@SuppressWarnings("serial")
public class ParentOfRootRequestedException extends Exception {

	public String toString() {
		return "Parent of root requested";
	}
}
