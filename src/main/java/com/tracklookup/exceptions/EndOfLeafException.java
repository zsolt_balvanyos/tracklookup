package com.tracklookup.exceptions;

@SuppressWarnings("serial")
public class EndOfLeafException extends Exception {
	private int channelId;
	private int start;
	
	public EndOfLeafException(int channelId, int start) {
		this.channelId = channelId;
		this.start = start;
	}

	public int getChannelId() {
		return channelId;
	}

	public int getStart() {
		return start;
	}
	
	public String toString() {
		return "Reached the end of leaf on channel " + channelId + ", leaf starts from " + start;
	}
}
