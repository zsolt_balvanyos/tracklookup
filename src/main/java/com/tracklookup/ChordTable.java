package com.tracklookup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ChordTable {
	final String CHORDTABLE = "." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "ChordTable.txt";
	final int NOTES = 0;
	final int CHORDS = 1; 
	final Map<Set<Integer>,Integer> table;
	Logger logger = LogManager.getLogger(ChordTable.class);
	
	/**
	 * Connects to the chord chordTable text file and reads in each lines.
	 * @throws IOException 
	 */
	public ChordTable() {
		table = new HashMap<Set<Integer>, Integer>();
	}

	@PostConstruct
	public void init() {
		try{
			BufferedReader br = new BufferedReader(new FileReader(new File(CHORDTABLE)));

			String line = "";

			while((line = br.readLine()) != null) {
				String[] cut = line.split("-");
				int root = Integer.parseInt(cut[CHORDS]);	
				
				Set<Integer> chord = new HashSet<Integer>();		
				for(String number: cut[NOTES].split(",")) {
					chord.add(Integer.parseInt(number));
				}		
				table.put(chord, root);
			}

			br.close();
		} catch (Exception e) {
			logger.error("ChordTable.txt file has not been found at " + CHORDTABLE);
		}
	}
	
	/**
	 * Looks up the root of the chord.
	 * If the chord is not found in the chordTable, returns 13.
	 * @param chord
	 * @return root note.
	 */
	public int getRoot(Set<Integer> chord) {
		if(chord.size() == 1) {
			Integer[] intArray = chord.toArray(new Integer[1]);
			return intArray[0];
		}
		
		if(chord.size() == 2) {
			return Collections.min(chord);
		}

		if(table.get(chord) == null) {
			return 13;
		}	
		return table.get(chord);
	}
}